// © 2018 the slacktricks Authors under the WTFPL license. See AUTHORS for the list of authors.

package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

var dataDir = flag.String("data", "data", "Where to save emoji")

type emojyResp struct {
	Emoji map[string]string
}

func main() {
	flag.Parse()
	f, err := ioutil.ReadAll(bufio.NewReader(os.Stdin))
	if err != nil {
		panic(err)
	}

	var resp emojyResp
	if err := json.Unmarshal(f, &resp); err != nil {
		panic(err)
	}

	count := 0
	for k, v := range resp.Emoji {
		if !strings.HasPrefix(v, "alias") {
			ext := filepath.Ext(v)
			resp, err := http.Get(v)
			if err != nil {
				panic(err)
			}
			fname := fmt.Sprintf("%s%s", k, ext)
			f, err := os.Create(filepath.Join(*dataDir, fname))
			defer f.Close()
			b, err := io.Copy(f, resp.Body)
			if err != nil {
				panic(err)
			}
			log.Printf("Saved %d bytes to %s", b, fname)
			count++
		}
	}
	log.Printf("Saved %d emojys", count)
}
